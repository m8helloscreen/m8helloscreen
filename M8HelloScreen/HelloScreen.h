#ifndef _HELLO_SCREEN_H
#define _HELLO_SCREEN_H

#define RGB565(r, g, b) ((r >> 3) << 11)| ((g >> 2) << 5)| ((b >> 3) << 0) 

void pattern();
void fillscreen(unsigned short color);
void skyblue();
void box(int x, int y, int w, int h, bool fill = false, bool fillfg = true);
void dualbox(int x, int y, int w, int h, bool fl);
void writetext(int x, int y, char* str);

extern unsigned int FrameBufferAddr;

#endif

