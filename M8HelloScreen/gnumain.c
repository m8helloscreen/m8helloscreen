#include <stdlib.h>

void start();
void pattern();
void skyblue();

void _start() {
    start;
}

void start() {
    //pattern();
    //void* funcadd = &skyblue;
    skyblue();
    typedef void (*FARPROC)();
    FARPROC funcadd = (FARPROC)&pattern;
    for (int i = 0; i < 40; i++)
    {
        funcadd();
        skyblue();
    }
    //return int(funcadd);
}

void pattern() {
    for (int loop = 12; loop > 0; loop--) {
        unsigned short* framebuffer = (unsigned short*)0x5D300000UL;
        for (int i = 0; i < 0xA8C00 / 2; i++) {
            *framebuffer = (unsigned short)((i + (loop << 2)) & 0xFFFF);
            framebuffer++;
        }
    }
    return;
}

void skyblue(){
    unsigned short* framebuffer = (unsigned short*)0x5D300000UL;
    for (int i = 0; i < 0xA8C00 / 2; i++) {
        *framebuffer = (unsigned short)0x3000;
        framebuffer++;
    }
}