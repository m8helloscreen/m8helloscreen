#ifndef _TINY_CRT_H
#define _TINY_CRT_H

#ifdef __cplusplus 
extern "C" { 
#endif 

void *  __cdecl Wmemset(void * _Dst, int _Val, unsigned int _Size);

#ifdef __cplusplus 
} 
#endif 




#endif
