#include "tinycrt.h"

void * __cdecl Wmemset (
                       void *dst,
                       int val,
                       unsigned int count
                       )
{
    void *start = dst;

    while (count--) {
        *(unsigned short *)dst = (unsigned short)val;
        dst = (unsigned short *)dst + 1;
    }
    /*unsigned short* start  = dst;
    for (; start < count + start; start++) {
        *start = (unsigned short)val;
    }*/
    return(start);
}