#include <stdlib.h>
#include <windows.h>
#include "helloscreen.h"
#include "tinycrt.h"

unsigned int FrameBufferAddr;

void pattern() {
    for (int loop = 12; loop > 0; loop--) {
        unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;//0x5D300000UL;
        for (int i = 0; i < 0xA8C00 / 2; i++) {
            *framebuffer = (unsigned short)((i + (loop << 2)) & 0xFFFF);
            framebuffer++;
#ifndef ARM
            // wait?
            //SleepEx(0, TRUE);
#endif
        }
    }
    return;
}

void fillscreen(unsigned short color) {
    // TODO: asm speed
    unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;//0x5D300000UL;
    for (int i = 0; i < 0xA8C00 / 2; i++) {
        *framebuffer = color;
        framebuffer++;
    }
}

void skyblue() {
    fillscreen(RGB565(0, 0x72, 0xBA));
}

void box(int x, int y, int w, int h, bool fill, bool fillfg) {
    unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;
    unsigned short* lefttopcorner = framebuffer + x + y * 720;
    if (fill) {
        for (int i = 0; i < h + 1; i++) {
            Wmemset(lefttopcorner + 1, (fillfg?0xFFFF:0), w - 1);
            lefttopcorner += 720;
        }
    } else {
        Wmemset(lefttopcorner, 0xFFFF, w + 1);
        Wmemset(lefttopcorner + h * 720, 0xFFFF, w + 1);
        for (int i = x + (y + 1) * 720; i < x + (y + h) * 720; i+=720) {
            *(framebuffer + i) = 0xFFFF;
            *(framebuffer + i + w) = 0xFFFF;
        }
    }
}

void dualbox(int x, int y, int w, int h, bool fl) {
    box(x, y, w, h, true, false); // Fill Cleanup
    box(x, y , w, h);
    if (fl) {
        box(x, y, w, 9);
        box(x + 2, y + 11, w - 4, h - 13);
    }
}

unsigned int MyFont[69] = {
    8796552, 2308290, 1030272, 146400, 1,
    100873112, 330, 352137162, 150156228, 589435185,
    748333380, 69830, 272765064, 71438466, 10378,
    139459716, 210120704, 1047552, 138412032, 36909840,
    490399278, 138547396, 1042424366, 487854367, 277816609,
    488127551, 488160318, 138551871, 488159790, 521094702,
    6297600, 71499968, 272697480, 32506848, 71573634,
    134357550, 1008719614, 589284676, 521715247, 487621694,
    521717295, 1041284159, 34651199, 488015406, 588840497,
    474091662, 488129040, 580029609, 1041269793, 588830577,
    597350001, 488162862, 35112495, 748340782, 580372015,
    487856686, 138547359, 488162865, 145278513, 358274609,
    581046609, 138547537, 1042419999, 1013127390, 562436193,
    516305295, 324, 1040187392, 2180
};

void writetext(int x, int y, char* str) {
    unsigned short* framebuffer = (unsigned short*)FrameBufferAddr;
    unsigned short* lefttopcorner = framebuffer + x + y * 720;
    //9100 FOR D=1 TO LEN(CO$):HN=FT(ASC(MID$(CO$,D,1))-28)
    //9110 IF HN=1 THEN XN=XN+2:9140
    //9120 FOR Y=0 TO 5: FOR X=0 TO 4:IF HN<>INT(HN/2)*2 THEN DRAW XN+X,YN+Y
    //9130 HN=INT(HN/2):NEXT X:NEXT Y:XN=XN+6
    //9140 NEXT D:RETURN
    while (*str != 0) {
        int hn = *str - 28;
        str++;
        if (hn < 0 || hn >= _countof(MyFont)) {
            continue;
        }
        if (hn == 4) {
            // space? tab?
            x += 2;
            continue;
        }
        hn = MyFont[hn];
        if (hn == 1) {
            //space? tab?
            x += 2;
            continue;
        }
        for (int iy = 0; iy < 6; iy++) {
            for (int ix = 0; ix < 5; ix++) {
                if (hn % 2 == 1) {
                    *(lefttopcorner + ix + iy * 720) = 0xFFFF;
                }
                hn = hn / 2;
            }
        }
        lefttopcorner += 6;
    }
}