    AREA .text, CODE, READONLY
		EXPORT myStackStartup
myStackStartup PROC

    ;LDR R0, =StackAddr
    ;MOV SP, R0
    ;LDR R0, =|?myStartup@@YAXXZ|
    ;SUB R0, R0, #0x30000000
    ;LDR R0, [R0]
    ;MOV PC, R0
    ;B |?start@@YAXXZ|
    MOV R0, #0x50000000
    ORR R0, R0, #0x100000
    ORR R0, R0, #0x800
    MOV SP, R0
    B |?start@@YAXXZ|
    ;B .
    ;B .

    IMPORT |?myStartup@@YAXXZ|
    IMPORT |?skyblue@@YAXXZ|
    IMPORT |?start@@YAXXZ|

;StackAddr
;    DCD 0x50100800

ENDP
    END
