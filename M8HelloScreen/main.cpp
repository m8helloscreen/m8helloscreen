#include <stdlib.h>
#include "HelloScreen.h"

void myStartup(void) {
    //.text:80044CAC                 MOV     R7, #0
    //.text:80044CB0                 MOV     R3, #480
    //.text:80044CB4                 MOV     R2, #720
    //.text:80044CB8                 MOV     R1, #DISP_16BPP_565
    //.text:80044CBC                 MOV     R0, #DISP_WIN1_DMA
    //.text:80044CC0                 STR     R7, [SP,#0x84+var_80]
    //.text:80044CC4                 STR     R7, [SP,#0x84+var_84]
    //.text:80044CC8                 BL      sub_80055A80
    //.text:80044CCC                 MOV     R3, #0x5D000000
    //.text:80044CD0                 ORR     R1, R3, #0x300000
    //.text:80044CD4                 MOV     R0, #1
    //.text:80044CD8                 BL      sub_800548D8

    //Disp_set_window_mode(DISP_WIN1_DMA, DISP_16BPP_565, LCD_WIDTH, LCD_HEIGHT, 0, 0); 720, 480
    //Disp_set_framebuffer(DISP_WIN1, IMAGE_FRAMEBUFFER_PA_START); // 0x5D300000UL

    for (int loop = 1; loop > 0; loop--) {
        unsigned short* framebuffer = (unsigned short*)0x5D300000UL;
        for (int i = 0; i < 0xA8C00 / 2; i++) {
            *framebuffer = (unsigned short)(i & 0xFFFF);
            framebuffer++;
        }
    }
    //pattern();
    return;
};

void start() {
    /*skyblue();
    typedef void (*FARPROC)();
    FARPROC funcadd = (FARPROC)&pattern;
    for (int i = 0; i < 40; i++)
    {
        funcadd();
        skyblue();
    }*/
    FrameBufferAddr = 0x5D300000UL;
    skyblue();
    box(1, 66, 158, 13);
    box(3, 68, 33, 9);
    writetext(5, 70, "START");
    writetext(2, 59, "BANXIAN`S OICQ NUM:32051976");
    dualbox(12, 7, 96, 71, true);
    writetext(32, 9, "FIRST DEMO");
    writetext(16, 20, "HELLO WORLD!");
    writetext(25, 27, "POWERFUL!!");
    writetext(16, 34, "[FONT DATA]\x1F");
    writetext(16, 41, "\x12\x13\x14\x15 !\22#$%&'()*");
    writetext(16, 48, "+,-./0123456789");
    writetext(16, 55, ":;<=>?@ABCDEFGH");
    writetext(16, 62, "IJKLMOPQRRSTUVW");
    writetext(16, 69, "XYZ[\\]^_`");
    //return int(funcadd);
}
